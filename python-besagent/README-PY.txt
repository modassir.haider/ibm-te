The script will do the following things:-
 
1) First of all, you need to place all DNS in hostname.txt file and it should be in column.
2) Script will check whether Server is UP or DOWN.
3) If server is UP, Then it will fetch the besagent and python package details to agent.txt and py.txt file respectively.
4) If the size of py.txt file is greater than zero MIB, then it will collect the python version to python-version.txt file.
5) If the size of py.txt file is zero MiB, Then it will write "not installed" to python-version.txt file.
6) At last, you will have all the python package version details in python-version.txt file in column.
7) First row from hostname.txt will point to 1st row in python-version.txt file. 2nd row with 2nd row...like that...  
