#!/bin/bash

ss="ssh -o LogLevel=Error"
oldfolder="backup/before-patch/"
folder="backup/after-patch/"

echo -e "Enter the server name which you are going to take backup & compare with the old backup:-"
read -a server
echo -e "the server name you entered is: \e[1;35m  $server \e[0m"


foldercreate=$(mkdir -p $folder$server)

function mountp {

	backup=$($ss $server "df -kh" > /dev/null 2>&1 > $folder$server/mountpoint.txt)
	# Above command is used to take the backup of mountpoint from the server and store under backup/after-patch/servername folder	
	awk '{print $1,$6}' $oldfolder$server/mountpoint.txt > point.txt
	awk '{print $1,$6}' $folder$server/mountpoint.txt > point1.txt
	compare=$( diff <(sort point.txt) <(sort point1.txt) > mp.txt)	
	#Compare old mountpoint with the  latest mountpoint and generate the output.	
	mp=mp.txt
	if [ -s "$mp" ]; 
	then

		echo -e "\e[1;34m MOUNT POINT - \e[1;31;5m  FAILED \e[0m"
		echo -e "\e[1;31m `cat mp.txt`  \e[0m"
		remove=$( rm mp.txt point.txt point1.txt )	
		#remove=$( rm mp.txt $oldfolder$server/point.txt $folder$server/point.txt ) 
	else
		echo -e "\e[1;34m MOUNT POINT - \e[1;32m  passed \e[0m" 
		remove=$( rm mp.txt point.txt point1.txt )	
	
	fi

}

function serv {

        backup=$($ss $server "svcs -a" > $folder$server/services.txt)
       	# Above command is used to take the backup of the services from the server and store under backup/after-patch/servername folder 
	awk '{print $1,$3}' $oldfolder$server/services.txt > serv.txt
        awk '{print $1,$3}' $folder$server/services.txt > serv1.txt
	grep -i 'maintenance' serv1.txt > main.txt
        compare=$( diff <(sort serv.txt) <(sort serv1.txt) > mp.txt)
       	#Now compare old services (backup taken before the patching) with the latest services and generate the output.  
	mp=mp.txt
        if [ -s "$mp" ]; 
	then

                echo -e "\e[1;34m SERVICES - \e[1;31;5m  failed \e[0m"
                echo -e "\e[1;31m `cat mp.txt`  \e[0m"
		remove=$( rm mp.txt serv.txt serv1.txt )
                
        else
                echo -e "\e[1;34m SERVICES - \e[1;32m  passed \e[0m"
		remove=$( rm mp.txt serv.txt serv1.txt )
                
        fi

	# below IF condition is used, if there is any maintenance is required to the service, it will generate an output.	
	maintenance=main.txt
	if [ -s "$maintenance" ]; 
	then

		echo -e "\e[1;34m REQUIRED - \e[1;31m  `cat main.txt` \e[0m"
		remove=$( rm -rf main.txt )
	
	else
		remove=$( rm -rf main.txt )	
	
	fi
}


function alldevice {

        backup=$($ss $server "iostat -en" > $folder$server/devices.txt)
	#Above command is used to take the backup of devices from the server and store under backup/after-patch/servername folder
        awk '{print $5}' $oldfolder$server/devices.txt > devs.txt
        awk '{print $5}' $folder$server/devices.txt > devs1.txt
        compare=$( diff <(sort devs.txt) <(sort devs1.txt) > mp.txt)
       	#Now compare old devices (backup taken before the patching) and latest devices and generate the output. 
	mp=mp.txt
        if [ -s "$mp" ]; 
	then

                echo -e "\e[1;34m DEVICES - \e[1;31;5m FAILED \e[0m"
                echo -e "\e[1;31m `cat mp.txt`  \e[0m"
                remove=$( rm mp.txt devs.txt devs1.txt )
                
        else
                echo -e "\e[1;34m DEVICES - \e[1;32m  passed \e[0m"
                remove=$( rm mp.txt devs.txt devs1.txt ) 
        fi

}


function phydisk {
	
	backup=$($ss $server "echo | format" > /dev/null 2>&1 > $folder$server/disk_phy.txt)
	#backup=$($ss $server "echo | format" > $folder$server/disk_phy.txt)
       	#Above command is used to take the backup of physical disk from the server and store under backup/after-patch/servername folder 
        cp $oldfolder$server/disk_phy.txt disk.txt
        cp $folder$server/disk_phy.txt disk1.txt
        compare=$( diff <(sort disk.txt) <(sort disk1.txt) > mp.txt)
       	#Now compare old physical_disk (backup taken before the patching) and latest physical_disk and generate the output. 
	disknumber="$(wc -l disk.txt | awk '{print $1}')"
        disknumber1="$(wc -l disk1.txt | awk '{print $1}')"
        mp=mp.txt
        if [ -s "$mp" ]; 
	then

                echo -e "\e[1;34m PHYSICAL DISK - \e[1;31;5m  FAILED \e[1;36m  ( going to verify once again ) \e[0m"
                echo -e "\e[1;31m `cat mp.txt`  \e[0m"
                remove=$( rm mp.txt disk.txt disk1.txt )

		#Below IF condition is used, If there will be any issue during sorting of number then again it will check and compare with the number of lines
                if (( $(nawk 'BEGIN {print ( '$disknumber' >= '$disknumber1' )}')   )); 
		then
                #if (( $(echo "$disknumber == $disknumber1" | bc -l) ));       then

                        echo -e "\e[1;34m PHYSICAL DISK - \e[1;32m  PASSED -- \e[1;36m ( \e[1;32m No worries \e[1;36m Looks like issue is with number of ordering, Just for confirmation, Please verify manually ) \e[0m"
                
		else
                        echo -e "\e[1;34m PHYSICAL DISK VERIFICATION - \e[1;31;5m  FAILED \e[0m"
                
		fi

        else
                echo -e "\e[1;34m PHYSICAL DISK - \e[1;32m  PASSED \e[0m"
                remove=$( rm mp.txt disk.txt disk1.txt )

        fi

}



function poollist {

        backup=$($ss $server "zpool list" > $folder$server/pool.txt)
       	#Above command is used to take the backup of pool from the server and store under backup/after-patch/servername folder 
	awk '{print $1}' $oldfolder$server/pool.txt > zpool.txt
        awk '{print $1}' $folder$server/pool.txt > zpool1.txt
        compare=$( diff <(sort zpool.txt) <(sort zpool1.txt) > mp.txt)
	#Now compare old pool (backup taken before the patching) with the latest pool and generate the output.	
        mp=mp.txt
        if [ -s "$mp" ]; 
	then

                echo -e "\e[1;34m POOL DIFFERENCE - \e[1;31;5m  FAILED \e[0m"
                echo -e "\e[1;31m `cat mp.txt`  \e[0m"
                remove=$( rm mp.txt zpool.txt zpool1.txt )

        else
                echo -e "\e[1;34m POOL DIFFERENCE- \e[1;32m  PASSED \e[0m"
                remove=$( rm mp.txt zpool.txt zpool1.txt )

        fi
	
	#
	cp $folder$server/pool.txt .
	#nawk '{print $7}' pool.txt > strat.txt
	nawk '{print $(NF-1) }' pool.txt > strat.txt

	sed 's/HEALTH//g' strat.txt > stat.txt
	sed 's/ONLINE//g' stat.txt > strat.txt
	#sed '/^[[:space:]]*$/d' strat.txt > stat.txt
	perl -ni -e 's/^ *//; s/ *$//; print if /\S/' strat.txt 
        nawk '!seen[$0]++' strat.txt > stats.txt
	# remove dublicate lines
	#if the pools are not in "ONLINE" state, it will generate the output with the error details.

	if [ -s stats.txt ]; 
	then

        	echo -e "\e[1;34m POOLS (ONLINE) - \e[1;31;5m  FAILED \e[0m"
        	foldercreate=$(mkdir break)
        	spliting=$(split -l 1 stats.txt break/)
        	#num="$(ls break/ | wc -l)"
        	listoftmp=$(ls -1 break/)
        	
		for tm in $listoftmp; do

	                grep `cat break/$tm` pool.txt > error.txt
        	        echo -e "\e[1;31m  `cat error.txt` \e[0m"       

		done;
        	
		#add health
		#shows the health of the pool	
		healths=$($ss $server "zpool status -xv" > health.txt)
		echo -e "\e[1;34m POOL HEALTH - \e[1;31m  `cat health.txt` \e[0m"	
		remove=$(rm -rf break stat.txt stats.txt strat.txt error.txt pool.txt health.txt)

	else

        	echo -e "\e[1;34m POOLS (ONLINE) - \e[1;32m  passed \e[0m"        
		#add health
		#shows the health of the pool	
		healths=$($ss $server "zpool status -xv" > health.txt)
		echo -e "\e[1;34m POOL HEALTH - \e[1;32m  `cat health.txt` \e[0m" 
		remove=$(rm stat.txt stats.txt strat.txt pool.txt health.txt)
	fi

}


function version {

        backup=$($ss $server "pkg list kernel" > /dev/null 2>&1 > 5_11.txt)
       	#take the backup of kernel version 
	if [ -s 5_11.txt ]; 
	then
        
	        movefile=$(mv 5_11.txt $folder$server/os-version.txt)
		grep -i kernel $oldfolder$server/os-version.txt | awk '{print $2}' > oldversion.txt
        	grep -i kernel $folder$server/os-version.txt | awk '{print $2}' > newversion.txt
        	oldkernel=`cat oldversion.txt`
        	newkernel=`cat newversion.txt`
        	#IF condition is used to check whether old backup kernel version is similar to the latest kernel version or not. 
		#if it matches, then fails. if not, then passed	
        	if (( $(nawk 'BEGIN {print ( '$oldkernel' == '$newkernel' )}')   )); 
		then

	                #echo -e "\e[1;34m KERNEL VERSION - \e[1;31;5m  FAILED \e[0m \e[5;36;7m  ( This is not the latest kernel ) \e[0m"
			echo -e "\e[1;34m KERNEL VERSION - \e[1;31;5m  FAILED \e[0m \033[4;36;7m ( This is not the latest kernel ) \033[0m"	
			remove=$(rm oldversion.txt newversion.txt )

	        else
	
        	        echo -e "\e[1;34m KERNEL VERSION - \e[1;32m  PASSED  \e[1;36m  ( Great, this is the updated kernel version) \e[0m"
                	remove=$(rm oldversion.txt newversion.txt )

	        fi

        else
                removefile=$(rm 5_11.txt)
                backup=$($ss $server "uname -v" > 5_10.txt)
                grep -i virtual 5_10.txt > 5_10-virtual.txt
		#Below IF condition is used for 5.10 virtual server. 
		if [ -s 5_10-virtual.txt ]; 
		then

                        backup=$($ss $server "ls /var/sadm/patch |grep 150400 |tail -1" > $folder$server/os-version.txt)
                        removefile=$(rm 5_10-virtual.txt 5_10.txt)
                        #root@ssrvw04cl:~# zoneadm list
                        #root@ssrvw04cl:~# /usr/local/config/bin/check_kernel_patch_level.sh supappd04

			cp $oldfolder$server/os-version.txt oldversion.txt
        		cp $folder$server/os-version.txt newversion.txt
                	oldkernel=`cat oldversion.txt`
        		newkernel=`cat newversion.txt`
			#IF condition is used to check whether old backup kernel version is similar to the latest kernel version or not.
                	#if it matches, then fails. if not, then passed 
		        if (( $(nawk 'BEGIN {print ( '$oldkernel' == '$newkernel' )}')   )); 
			then

		                echo -e "\e[1;34m KERNEL VERSION - \e[1;31;5m  FAILED  \e[1;36m  ( This is not the latest kernel ) \e[0m"
	        	        remove=$(rm oldversion.txt newversion.txt )

		        else

		                echo -e "\e[1;34m KERNEL VERSION - \e[1;32m  PASSED  \e[1;36m  ( Great, this is the updated kernel version) \e[0m"
	        	        remove=$(rm oldversion.txt newversion.txt )

		        fi		


                else

                        movefile=$(mv 5_10.txt $folder$server/os-version.txt)

			cp $oldfolder$server/os-version.txt oldversion.txt
        		cp $folder$server/os-version.txt newversion.txt
                	oldkernel=`cat oldversion.txt`
        		newkernel=`cat newversion.txt`
			#IF condition is used to check whether old backup kernel version is similar to the latest kernel version or not.
                	#if it matches, then fails. if not, then passed 
			if (( $(nawk 'BEGIN {print ( '$oldkernel' == '$newkernel' )}')   )); 
			then

		                echo -e "\e[1;34m KERNEL VERSION - \e[1;31;5m  FAILED  \e[1;36m  ( This is not the latest kernel ) \e[0m"
	        	        remove=$(rm oldversion.txt newversion.txt )

		        else

		                echo -e "\e[1;34m KERNEL VERSION - \e[1;32m  PASSED  \e[1;36m  ( Great, this is the updated kernel version) \e[0m"
	        	        remove=$(rm oldversion.txt newversion.txt )

		        fi
                        removefile=$(rm 5_10-virtual.txt)

                fi
        fi



}

function hardwareissue {

	hwissue=$($ss $server "fmadm faulty" > /dev/null 2>&1 > fault.txt 2> stderr.txt)
	# Here it will check, is there any issue on hardware or software. 	
	mps=fault.txt
        mp=stderr.txt
        if [ -s "$mp" ]; 
	then

                echo -e "\e[1;34m fmadm faulty - \e[1;31m  `cat stderr.txt` \e[0m"
                remove=$( rm stderr.txt fault.txt )

        elif [ -s "$mps" ]; 
	then
		
		echo -e "\e[1;34m HARDWARE/SOFTWARE ISSUE - \e[1;31;5m  YES (error found) \e[0m"
		remove=$( rm stderr.txt fault.txt )
		
	else
		
		echo -e "\e[1;34m HARDWARE/SOFTWARE ISSUE - \e[1;32m  NO \e[0m"
                remove=$( rm stderr.txt fault.txt )

        fi

}

function nfsmount {

	$ss $server "showmount -e" > /dev/null 2>&1 > $folder$server/showmount.txt 2> stderr.txt
        cp $oldfolder$server/showmount.txt show.txt
        cp $folder$server/showmount.txt show1.txt
        compare=$( diff <(sort show.txt) <(sort show1.txt) > mps.txt)
        mps=mps.txt
        mp=stderr.txt
        if [ -s "$mp" ];
        then

                echo -e "\e[1;34m NFS - \e[1;31m  `cat stderr.txt` \e[0m"
                remove=$( rm stderr.txt show.txt show1.txt mps.txt )
        
	elif [ -s "$mps" ];
        then

                echo -e "\e[1;34m NFS - \e[1;31;5m  Failed \e[0m"
		echo -e "\e[1;31m `cat mps.txt`  \e[0m"
                remove=$( rm stderr.txt show.txt show1.txt mps.txt )

        else

                echo -e "\e[1;34m NFS - \e[1;32m  PASSED \e[0m"
                remove=$( rm stderr.txt show.txt show1.txt mps.txt )

        fi
}

function packages {

	backup=$($ss $server "pkg list" > /dev/null 2>&1 > pkg_5_11.txt)
        #take backup of all installed packages
        if [ -s pkg_5_11.txt ];
        then
		awk '{ print $1 }' pkg_5_11.txt > $folder$server/package.txt

		cp $oldfolder$server/package.txt pk.txt
        	cp $folder$server/package.txt pk1.txt
        	compare=$( diff <(sort pk.txt) <(sort pk1.txt) > mps.txt)
		mp=mps.txt
       
        	if [ -s "$mp" ];
        	then

			echo -e "\e[1;34m PACKAGES - \e[1;31;5m FAILED \e[0m"
                	echo -e "\e[1;31m `cat mps.txt`  \e[0m"
			removefile=$( rm pk.txt pk1.txt mps.txt pkg_5_11.txt )
		else
			echo -e "\e[1;34m PACKAGES - \e[1;32m  PASSED \e[0m"
                	removefile=$( rm pk.txt pk1.txt mps.txt pkg_5_11.txt )
		fi                

	else
	
		backup_package=$( $ss $server "pkginfo -l | grep PKGINST " > pkg_5_10.txt )
		awk '{ print $2 }' pkg_5_10.txt > $folder$server/package.txt

		cp $oldfolder$server/package.txt pk.txt
        	cp $folder$server/package.txt pk1.txt
        	compare=$( diff <(sort pk.txt) <(sort pk1.txt) > mps.txt)
		mp=mps.txt
       
        	if [ -s "$mp" ];
        	then
	                
			echo -e "\e[1;34m PACKAGES - \e[1;31;5m FAILED \e[0m"	
			echo -e "\e[1;31m `cat mps.txt`  \e[0m"
                	removefile=$( rm pk.txt pk1.txt mps.txt pkg_5_10.txt pkg_5_11.txt )	

		else
			echo -e "\e[1;34m PACKAGES - \e[1;32m  PASSED \e[0m"
			removefile=$( rm pk.txt pk1.txt mps.txt pkg_5_10.txt pkg_5_11.txt )                	
		fi	
	
		
	fi
}


function zones {

        backup=$($ss $server "zoneadm list" > $folder$server/zone.txt)
	backup=$($ss $server "zoneadm list -cv" > $folder$server/zone-cv.txt)
        #take backup of global zone
	cp $oldfolder$server/zone.txt zone1.txt
	cp $oldfolder$server/zone-cv.txt zone11.txt
	cp $folder$server/zone.txt zone2.txt
	cp $folder$server/zone-cv.txt zone22.txt
	diff <(sort zone1.txt) <(sort zone2.txt) > mp.txt
	
	awk '{print substr($0, index($0, $2))}' zone11.txt > zone111.txt
	awk '{print substr($0, index($0, $2))}' zone22.txt > zone222.txt
	# remove first 2 lines	
	nawk '{$1=$1}1' zone111.txt > zone11.txt
	nawk '{$1=$1}1' zone222.txt > zone22.txt
	# remove space between the column
	diff <(sort zone11.txt) <(sort zone22.txt) > mps.txt
	mp=mp.txt	
	mps=mps.txt
	
	#if [ -s "$mp" ] && [ -s "$mps" ]; then
	if [ -s "$mp" ] || [ -s "$mps" ]; then
	
	  	echo -e "\e[1;34m ZONE - \e[1;31;5m FAILED \e[0m"
                echo -e "\e[1;31m `cat mp.txt`  \e[0m"
               	echo -e "\e[1;31m `cat mps.txt`  \e[0m" 
		removefile=$( rm zone1.txt zone2.txt zone11.txt zone22.txt zone111.txt zone222.txt mp.txt mps.txt)
	
	else

		echo -e "\e[1;34m ZONE - \e[1;32m  PASSED \e[0m"
		removefile=$( rm zone1.txt zone2.txt zone11.txt zone22.txt zone111.txt zone222.txt mp.txt mps.txt)
	
	fi
}


mountp
serv
alldevice
phydisk
poollist
version
hardwareissue
nfsmount
packages
zones



