#!/bin/bash

dates=$(date +'%d-%m-%Y-%T')
path="/crm/cadm/bin/crossreference.txt"
physicalpath="/etc/serialports"

#now="(date +'%d-%m-%Y')"

function filebackup {


        echo -e "Enter the server name which you are going to decommission:-"
        read -a db
        echo "the server name you entered is: ${db[@]}"

        tlen=${#db[@]}

        for ((i=0; i<${tlen}; i++));
        do



        filebackup=$( cp $path $path-$dates )


                comment=$( perl -pi -e "s/^${db[$i]}/#${db[$i]}/g" $path )
                #addline=$( sed -i "/${db[$i]}/s/$/ -  server is going to decommission/" $path )

                volume=$( grep ${db[$i]} $path | awk '{print $3}' )
                #echo $volume

                if [ PHY == $volume ];
                then
                        filebackup=$( cp $physicalpath $physicalpath-$dates )
                        echo "${db[$i]} is a physical machine, So going to make changes in $physicalpath"
                        comment=$( perl -pi -e "s/^${db[$i]}/#${db[$i]}/g" $physicalpath )

                else

                        echo "${db[$i]} is a virtual machine"

                fi


        done;
}

filebackup

# the above script will run on Solaris Server
