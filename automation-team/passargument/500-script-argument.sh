#!/bin/bash

dates=$(date +'%d-%m-%Y-%T')
path="/crm/cadm/bin/crossreference.txt"
physicalpath="/etc/serialports"

jumpserver=ssrv194bw

#now="(date +'%d-%m-%Y')"
hostname=$@


function filebackup {


        for i in $hostname;
        do


        	filebackup=$( ssh -o StrictHostKeyChecking=no $jumpserver -T "cp $path $path-$dates" )


                comment=$( ssh -o StrictHostKeyChecking=no $jumpserver -T  "perl -pi -e 's/^$i/#$i/g' $path" )
                #addline=$( sed -i "/${db[$i]}/s/$/ -  server is going to decommission/" $path )
		volume=$( ssh -o StrictHostKeyChecking=no $jumpserver -T "grep $i $path" > backup.txt)
		physical=$( awk '{print $3}' backup.txt )
                #volume=$( ssh $jumpserver -T "grep $i $path | awk '{print $3}'" )
                #echo $volume

                if [ PHY == `echo $physical` ];
		then
                        filebackup=$(ssh -o StrictHostKeyChecking=no $jumpserver -T  "cp $physicalpath $physicalpath-$dates" )
                        echo "$i is a physical machine, So going to make changes in $physicalpath"
                        comment=$( ssh -o StrictHostKeyChecking=no $jumpserver -T  "perl -pi -e 's/^$i/#$i/g' $physicalpath" )
			sleep 5s
                else

                        echo "$i is a virtual machine"

                fi


        done;
}

function rem {

	remove=$( rm backup.txt )

}


filebackup
rem

# the above script will run on Solaris Server
