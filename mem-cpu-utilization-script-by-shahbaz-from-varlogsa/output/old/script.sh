#!/bin/bash

function maxcpu {

	for pk in `ls -tr /var/log/sa/sa* | grep sar`
	do

		echo $pk > new
		sed -i 's#/var/log/sa/##g' new
		i=`cat new`
		echo $i


		getlinenumber=$( grep -n Average: $pk | grep -i cpu | grep -Eo '[[:digit:]]{1,4}' > num.txt )
		
		#awk '/CPU/{f=1} /Average/{f=0;print} f' sar01 | grep -v Average
		# Or you can use above command
		# You can use grep -E to access the extended regular expression syntax( Same as egrep)
		# Here "-o" is used to only output the matching segment of the line, rather than the full contents of the line.
		# https://askubuntu.com/questions/184204/how-do-i-fetch-only-numbers-in-grep
		getcpudetail=$( grep -B `cat num.txt` -i cpu $pk | grep -B `cat num.txt` Average > cpu.txt )

		arrayName=(12 01 02 03 04 05 06 07 08 09 10 11)

		# Loop through all elements in the array
		for j in "${arrayName[@]}"
		do

			am=$( grep ^$j cpu.txt | grep -i am | awk '{print $13}' > number )
			am=$( cat number | sort -nr -t":" -k3 -k4 -k5 | tail -n 1 > sub.txt )
			am=$( echo "100 - `cat sub.txt`" | bc -l >> cpu-sar-am )
	
			pm=$( grep ^$j cpu.txt | grep -i pm | awk '{print $13}' > number1 )
			pm=$( cat number1 | sort -nr -t":" -k3 -k4 -k5 | tail -n 1 > sub1.txt )
			pm=$( echo "100 - `cat sub1.txt`" | bc -l >> cpu-sar-pm )

		done;

		cpulist=$( cat cpu-sar-am cpu-sar-pm > cpu-$i-am-pm )
		remove=$( rm cpu-sar-am cpu-sar-pm )
		#mv=$( mv cpu-$i-am-pm /root/mod/output/ ) 
	done;

}


function maxmemory {

	for kg in `ls -tr /var/log/sa/sa* | grep sar`
	do
		#pk=/var/log/sa/sar01
		echo $kg > new1
		sed -i 's#/var/log/sa/##g' new1
		k=`cat new1`
		echo $k

		getmemdetail=$( awk '/memused/{f=1} /Average/{f=0;print} f' $kg | grep -v Average > memused.txt )

		arrayNam=(12 01 02 03 04 05 06 07 08 09 10 11)

		for l in "${arrayNam[@]}"
		do

			am=$( grep ^$l memused.txt | grep -i am | awk '{print $5}' | sed -e '/memused/d' > memnumber )
  			am=$( cat memnumber | sort -nr -t":" -k3 -k4 -k5 | head -n 1 > memsub.txt )
			am=$( echo "100 - `cat memsub.txt`" | bc -l >> mem-sar-am )		
			
			pm=$( grep ^$l memused.txt | grep -i pm | awk '{print $5}' | sed -e '/memused/d' > memnumber1 )
        		pm=$( cat memnumber | sort -nr -t":" -k3 -k4 -k5 | head -n 1 > memsub1.txt )
        		pm=$( echo "100 - `cat memsub.txt`" | bc -l >> mem-sar-pm )

		done;

		memlist=$( cat mem-sar-am mem-sar-pm > mem-$k-am-pm )
		remove=$( rm mem-sar-am mem-sar-pm )
		#mv=$( mv mem-$k-am-pm /root/mod/output/ )	

	done;

}


function rem { 

	fileremove=$( rm -rf num.txt cpu.txt number sub.txt number1 sub1.txt new ) 
	fileremove=$( rm -rf memused.txt memnumber memsub.txt memnumber1 memsub1.txt new1 )

}


maxcpu
maxmemory
rem



# https://stackoverflow.com/questions/22221277/bash-grep-between-two-lines-with-specified-string
# grep all the content between two lines
# https://stackoverflow.com/questions/13634429/in-linux-shell-script-how-do-i-print-the-largest-and-smallest-values-of-a-array
# https://www.geeksforgeeks.org/sorting-the-array-in-bash-using-bubble-sort/
# https://www.unix.com/shell-programming-and-scripting/136427-how-sort-floating-numbers-using-sort-command.html
# sort -nr -t":" -k3 -k4 -k5

