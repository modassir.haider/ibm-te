#!/bin/bash
dates=$(date +'%d-%m-%Y-%T')
path="/etc/resolv.conf"

while true
do
  read -r line1 <&3 || break
  read -r line2 <&4 || break
  read -r line3 <&5 || break
  read -r line4 <&6 || break

  backup=$(ssh $line1 -T "cp $path $path-$dates.bkp")
  del=$(ssh $line1 -T "sed -i '/nameserver/d' $path")
  addDns=$(ssh $line1 -T "echo 'nameserver $line2' >> $path")
  addDns=$(ssh $line1 -T "echo 'nameserver $line3' >> $path")
  addDns=$(ssh $line1 -T "echo 'nameserver $line4' >> $path")
echo $line1
done 3<hostname.txt 4<dn1.txt 5<dn2.txt 6<dn3.txt

# keep all hostname in file named hostname.txt
# keep all DN1 column in file named dn1.txt
# keep all DN2 column in file named dn2.txt
# keep all DN3 column in file named dn3.txt

