#!/bin/bash


for server in `cat hostname.txt`;
do


	ping -c 3 $server > /dev/null 2>&1
	if [ $? -ne 0 ]
	then

   		echo "$server is down" >> server-down.txt
		echo 'down' >> total-memory.txt
		echo 'down' >> used-memory.txt
		echo 'down' >> current-memory-percentage.txt
		echo 'down' >> memory-percentage-after-clearing-cache.txt


	else


		ssh $server -T "free -wmh | awk 'NR == 2'" > new

		awk '{print $2}' new >> total-memory.txt 
		# total memory

		awk '{print $3}' new >> used-memory.txt
		#ssh $i -T "free -wmh | awk 'NR == 2 {print $3}'"
		# used memory

		#ssh $i -T "free -t | awk 'FNR == 2 {print $3/$2*100}'"

		awk '{print $3/$2*100}' new >> current-memory-percentage.txt
		# current memory utilization
		
		ssh $server -T "sync; echo 3 > /proc/sys/vm/drop_caches"
		# clear cache

		ssh $server -T "free -wmh | awk 'NR == 2'" > new

		awk '{print $3/$2*100}' new >> memory-percentage-after-clearing-cache.txt
		# memory utilization in percentage after clearing cache.
	fi


done
# https://www.2daygeek.com/linux-check-cpu-memory-swap-utilization-percentage/
