#!/bin/bash


for server in `cat hostname.txt`;
do

        ping -c 3 $server > /dev/null 2>&1
        if [ $? -ne 0 ]
        then

                echo "$server is down" >> BESAgent.txt
                echo "$server is down" >> python-version.txt

        else

                scriptrun=$( ssh $server -T "rpm -qa | grep -i bESAgent" > agent.txt )

                if [ -s agent.txt ]; then

                        echo 'yes' >> BESAgent.txt
                else
                        echo 'no' >> BESAgent.txt

                fi


                scriptrun=$( ssh $server -T "rpm -qa python" > py.txt )

                if [ -s py.txt ]; then
                        scriptrun=$( ssh $server -T "rpm -qa python" >> python-version.txt )

                else
                        echo 'not installed' >> python-version.txt

                fi
        fi


done;
