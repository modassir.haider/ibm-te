#!/bin/bash

ss="ssh -o LogLevel=Error"
folder="backup/before-patch/"

echo -e "Enter the server name which you are going to take backup:-"
read -a server
echo "the server name you entered is: $server"

function serverback {
	
	foldercreate=$(mkdir -p $folder$server)
	# Below 6 lines are used to take the backup from the server and store under backup/before-patch/servername folder	
	backup=$($ss $server "df -kh" > /dev/null 2>&1 > $folder$server/mountpoint.txt)
	backup=$($ss $server "svcs -a" > /dev/null 2>&1 > $folder$server/services.txt)
	backup=$($ss $server "iostat -en" > /dev/null 2>&1 > $folder$server/devices.txt)
	backup=$($ss $server "echo | format" > /dev/null 2>&1 > $folder$server/disk_phy.txt)
	backup=$($ss $server "zpool list" > $folder$server/pool.txt)
	backup=$($ss $server "showmount -e" > /dev/null 2>&1 > $folder$server/showmount.txt)
	backup=$($ss $server "zoneadm list" > $folder$server/zone.txt)
	backup=$($ss $server "zoneadm list -cv" > $folder$server/zone-cv.txt)	
	backup=$($ss $server "pkg list kernel" > /dev/null 2>&1 > 5_11.txt)
	# if condition is used to get the kernel version from the server based on solaris 5.10 and 5.11.	
	if [ -s 5_11.txt ]; then
        	movefile=$(mv 5_11.txt $folder$server/os-version.txt)
        	echo "fine"	
		backup_package=$( $ss $server "pkg list" > pkg.txt )
		awk '{ print $1 }' pkg.txt > $folder$server/package.txt
		removefile=$(rm pkg.txt)
	else
		
		backup_package=$( $ss $server "pkginfo -l | grep PKGINST " > pkg.txt )
		awk '{ print $2 }' pkg.txt > $folder$server/package.txt	
		removefile=$(rm 5_11.txt pkg.txt)
        	backup=$($ss $server "uname -v" > 5_10.txt)
        	grep -i virtual 5_10.txt > 5_10-virtual.txt
        	if [ -s 5_10-virtual.txt ]; then
				
                	backup=$($ss $server "ls /var/sadm/patch |grep 150400 |tail -1" > $folder$server/os-version.txt)
			removefile=$(rm 5_10-virtual.txt 5_10.txt)
                	#root@ssrvw04cl:~# zoneadm list
                	#root@ssrvw04cl:~# /usr/local/config/bin/check_kernel_patch_level.sh supappd04

        	else

                	movefile=$(mv 5_10.txt $folder$server/os-version.txt)
                	removefile=$(rm 5_10-virtual.txt)
                	echo 'fine'
        	fi
	fi
}

serverback



