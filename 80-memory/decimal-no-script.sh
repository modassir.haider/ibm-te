#!/bin/bash

value=16

function mem {

for server in `cat hostname.txt`;
do
	getmemory=$(ssh $server -T "free -m | awk 'NR == 2'" > new)

	awk '{print $3/$2*100}' new > current-memory-percentage.txt
	utilization=`cat current-memory-percentage.txt`
	usag=$(cat current-memory-percentage.txt)
	if (( $(echo "$utilization > $value" | bc -l) ));	then
		echo 'done'
		echo "Earlier $server was utilizing $usag%" >> memory-percentage-cache.txt;
		clearcache=$(ssh $server -T "sync; echo 3 > /proc/sys/vm/drop_caches");
		
		#echo "Earlier it was utilizing $usag%" >> memory-percentage-cache.txt; 
		getmemory=$(ssh $server -T "free -m | awk 'NR == 2'" > new);
		echo "After cleared memory cache for server $server. please get the current memory utilization in %" >> memory-percentage-cache.txt;
		awk '{print $3/$2*100}' new >> memory-percentage-cache.txt;
		echo "-------------------------" >> memory-percentage-cache.txt;
		echo " " >> memory-percentage-cache.txt;
		#cat current-memory-percentage.txt | mailx -s "$server - memory details" 'mohaide1@in.ibm.com';

	else
		echo "$server is below threshold level"

	fi

done;
}

function del {

	cat memory-percentage-cache.txt | mailx -s "LINUX SERVER - memory details" 'mohaide1@in.ibm.com';
	rm current-memory-percentage.txt new memory-percentage-cache.txt
}

mem
#del


# You can do it using Bash's numeric context:
# if (( $(echo "$result1 > $result2" | bc -l) )); then
# bc will output 0 or 1 and the (( )) will interpret them as false or true respectively.
