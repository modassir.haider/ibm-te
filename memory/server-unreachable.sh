#!/bin/bash

for server in `cat hostname.txt`
do
#if nc -w $server 22 ; then
#    echo "$server ✓"
#else
#    echo "$server ✗"
#fi


ping -c 3 $server > /dev/null 2>&1
if [ $? -ne 0 ]
then
   
   echo "$server is down"

else
   echo "$server is up"

fi

done;


# echo $? means if output is 0, it means last executed command successfully. If last executed command gives some error then it means, when you run echo $?, it will give some value
# https://unix.stackexchange.com/questions/56340/bash-script-to-detect-when-my-server-is-down-or-offline
