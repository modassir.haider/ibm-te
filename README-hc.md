the script will do following things:-

1) First of all, Script will check whether Server is UP or DOWN.
2) If server is UP, then it will check whether HC-Linux-SSH-SUDO-V7.0.sh  exists or not.
3) If file exists, run the script and fetch the output file to the JEM server.
4) If file does not exist, it will copy HC-Linux-SSH-SUDO-V7.0.sh file from the JEM server to the selected server and run the script and fetch the output file back to the JEM server.
5) At last, it will ZIP the folder where all the ouput files are placed.

# we need to run HC-Linux-SSH-SUDO-V7.0.sh script on remote servers.
